package tests;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import Pages.HomePage;
import Pages.LoginPage;

public class Login extends TestBase {
	
	LoginPage loginin;
	HomePage home;
	utilities.ExcelReader excelReaderOpreation;
	
	@DataProvider (name="loginData")
	public Object[] []  userlogin() throws Exception
		{
		utilities.ExcelReader excelReader =new utilities.ExcelReader(System.getProperty("user.dir")+"/DDT/LoginData.xlsx");
		return excelReader.getRow(1, 2) ;	
		}
	
	@Test(dataProvider="loginData")
	public void SignIn (String mail, String pass) throws InterruptedException{
		loginin = new LoginPage(driver);
		home = new HomePage(driver);
		loginin.Login(mail, pass);
		Thread.sleep(5000);
		home.ClickOnKidsPlace();
	}
	
	

}
