package tests;

import java.awt.AWTException;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;

import Pages.CreateCalnder;
import Pages.EventData;
import junit.framework.Assert;

public class CreateEvent  extends TestBase{
	
	CreateCalnder calnder;
	EventData event;
	
	
	@Test
	public void OpenCalnder() throws InterruptedException, AWTException{
		calnder = new CreateCalnder(driver);
		event = new EventData(driver);
		Thread.sleep(3000);
		calnder.ClickOnCalnder();
		
		Thread.sleep(3000);
		event.ClickOnCreateEvent();
		event.CreateEvent();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Assert.assertTrue(event.CreateBtn.getText().contains("event"));

	}
	

}
