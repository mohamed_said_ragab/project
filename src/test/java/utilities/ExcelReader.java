package utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReader {
	public String filepath;
public ExcelReader (String filepath){this.filepath = filepath;
	
}
	static FileInputStream fis =null ;
	public FileInputStream getFileInputStream(){
		
		File srcfile = new File(filepath);
		
		try {
			fis = new FileInputStream(srcfile);
		} catch (FileNotFoundException e) {
			System.out.println("testdata file not found . check file Path of excel sheet");
			System.exit(0);
		}
	return fis ;
		
		
	}

	public String [][]getRow (int rownumber, int count  ) throws IOException{
		fis = getFileInputStream();
		
		XSSFWorkbook  wb = new XSSFWorkbook(fis);
		XSSFSheet sheet =  wb.getSheetAt(0);
		XSSFRow row = sheet.getRow(rownumber);
		String [][]rowData = new String [1][count];
		for (int i = 0; i < count; i++) {
			rowData[0][i]=row.getCell(i).toString();
		}
	
		wb.close();
		return rowData;
}
}