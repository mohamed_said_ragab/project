package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import tests.TestBase;

public class HomePage extends PageBase {

	public HomePage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	@FindBy(xpath = "//div[@class='institution__name'][contains(.,'Kids Palace')]")
	WebElement KidsPalace;
	
	
	
	public void ClickOnKidsPlace(){
		
		KidsPalace.click();
		
	}

}
