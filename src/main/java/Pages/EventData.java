package Pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class EventData extends PageBase {

	public EventData(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	
	@FindBy(name="createEventBtn")
public	WebElement CreateBtn;
	
	@FindBy(id="eventTitle")
	WebElement title;
	@FindBy(id="eventDescription")
	WebElement DESC;
	
	@FindBy(xpath = "//ng-select[@id='Recipients']//input[@type='text']")
	WebElement Recipients;
	
	@FindBy(xpath = "//span[contains(.,'Kids Palace (Children and staff)')]")
	WebElement Recipientclick;
	@FindBy(xpath = "//prt-date-picker[@class='ng-untouched ng-dirty ng-invalid']//div[@class='p-relative input_form clickable']")
	WebElement Date;

	@FindBy(xpath = "(//span[@ng-reflect-day='[object Object]'][contains(.,'30')])[2]")
	WebElement day30;
	
	@FindBy(xpath="//div[@class='form-group dropdown-form time-picker']//button[@type='button']")
	WebElement start;
	
	@FindBy(xpath="/html/body/bs-dropdown-container/div/ul/timepicker/table/tbody/tr[1]/td[1]/a")
	WebElement upfrom;
	@FindBy(xpath="//div[@class='row']//div[2]//prt-time-picker[1]//div[1]//button[1]")
	WebElement end;
@FindBy(xpath="/html/body/bs-dropdown-container/div/ul/timepicker/table/tbody/tr[1]/td[1]/a")

WebElement endup;

@FindBy(xpath="/html/body/app-root/app-main-layout/main/app-create-event/div/div[2]/form/div/div/div[3]/div[2]/button[2]")
WebElement save;
	

	
	  
     
      
	
	public void ClickOnCreateEvent(){
		
		CreateBtn.click();
		

}
	public void CreateEvent() throws InterruptedException, AWTException{
		
		setTextElementText(title, "hi");
		setTextElementText(DESC, "HELLO");
	//	Select Recipient = new Select(Recipients);
		Thread.sleep(5000);
		Recipients.click();
		Thread.sleep(5000);
		Recipientclick.click();
		Thread.sleep(5000);
		Date.click();
		Thread.sleep(3000);
		day30.click();
		Thread.sleep(3000);
	
		start.click();
		Thread.sleep(2000);
		Robot rb =new Robot();
		rb.keyPress(KeyEvent.VK_CONTROL);
		rb.keyPress(KeyEvent.VK_END);
		rb.keyRelease(KeyEvent.VK_CONTROL);
		rb.keyRelease(KeyEvent.VK_END);
		
		
		upfrom.click();
		upfrom.click();
		upfrom.click();
		upfrom.click();
		Thread.sleep(3000);
		end.click();
		endup.click();
		endup.click();
		endup.click();
		endup.click();
		endup.click();
		endup.click();
		Thread.sleep(3000);
		
		save.click();
		
		
		
		
		
	}

}
